/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-01-28     vandoul       the first version
 */
#ifndef APPLICATIONS_DRV_EPD_H_
#define APPLICATIONS_DRV_EPD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <rtthread.h>
#include <stdint.h>
#include "drv_epd_if.h"

enum {
    EPD_LUT_UPDATE_FULL = 0,
    EPD_LUT_UPDATE_PARTIAL,
    EPD_LUT_UPDATE_MAX_NUM,
};

// EPD2IN13 commands
#define EPD_CMD_DRIVER_OUTPUT_CONTROL                       0x01
#define EPD_CMD_BOOSTER_SOFT_START_CONTROL                  0x0C
#define EPD_CMD_GATE_SCAN_START_POSITION                    0x0F
#define EPD_CMD_DEEP_SLEEP_MODE                             0x10
#define EPD_CMD_DATA_ENTRY_MODE_SETTING                     0x11
#define EPD_CMD_SW_RESET                                    0x12
#define EPD_CMD_TEMPERATURE_SENSOR_CONTROL                  0x1A
#define EPD_CMD_MASTER_ACTIVATION                           0x20
#define EPD_CMD_DISPLAY_UPDATE_CONTROL_1                    0x21
#define EPD_CMD_DISPLAY_UPDATE_CONTROL_2                    0x22
#define EPD_CMD_WRITE_RAM                                   0x24
#define EPD_CMD_WRITE_VCOM_REGISTER                         0x2C
#define EPD_CMD_WRITE_LUT_REGISTER                          0x32
#define EPD_CMD_SET_DUMMY_LINE_PERIOD                       0x3A
#define EPD_CMD_SET_GATE_TIME                               0x3B
#define EPD_CMD_BORDER_WAVEFORM_CONTROL                     0x3C
#define EPD_CMD_SET_RAM_X_ADDRESS_START_END_POSITION        0x44
#define EPD_CMD_SET_RAM_Y_ADDRESS_START_END_POSITION        0x45
#define EPD_CMD_SET_RAM_X_ADDRESS_COUNTER                   0x4E
#define EPD_CMD_SET_RAM_Y_ADDRESS_COUNTER                   0x4F
#define EPD_CMD_TERMINATE_FRAME_READ_WRITE                  0xFF

void drv_epd_setlut(rt_device_t dev, int lut);
void drv_epd_init(rt_device_t dev, int lut);
void drv_epd_set_memory_area(rt_device_t dev, int x_start, int y_start, int x_end, int y_end);
void drv_epd_set_memory_pointer(rt_device_t dev, int x, int y);
void drv_epd_set_frame_memory(rt_device_t dev, const uint8_t* img_buf, int x, int y, int img_w, int img_h);
void drv_epd_clear_frame_memory(rt_device_t dev, uint8_t color);
void drv_epd_draw_color(rt_device_t dev, int x, int y, uint8_t color);
void drv_epd_display_frame(rt_device_t dev);
void drv_epd_sleep(rt_device_t dev);

#ifdef __cplusplus
}
#endif

#endif /* APPLICATIONS_DRV_EPD_H_ */
