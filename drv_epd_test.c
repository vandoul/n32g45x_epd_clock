/************************************************************
** @brief   : drv_epd_test
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-01
** @version : v1.0.0
** @note    : drv_epd_test.c
***********************************************************/
#include "drv_epd.h"
#include <stdlib.h>

static void epd_usage(void)
{
    rt_kprintf("Usage: epd <dev> <cmd> [param]\r\n");
    rt_kprintf("\tinit  - init epd device\r\n");
    rt_kprintf("\tclear - clear screen\r\n");
    rt_kprintf("\tpoint - draw point on coordinate\r\n");
}
int epd_test(int argc, char *argv[])
{
    if(argc == 1) {
        epd_usage();
        return 0;
    }
    rt_device_t dev = rt_device_find(argv[1]);
    if(dev == RT_NULL) {
        rt_kprintf("%s not found!\r\n", argv[1]);
        return 0;
    }
    if(rt_strcmp(argv[2], "init") == 0) {
        drv_epd_init(dev, EPD_LUT_UPDATE_FULL);
        return 0;
    } else if(rt_strcmp(argv[2], "lut") == 0) {
        int lut = atoi(argv[3]);
        if(lut==EPD_LUT_UPDATE_PARTIAL) {
            rt_kprintf("lut:partial\r\n");
            drv_epd_init(dev, EPD_LUT_UPDATE_PARTIAL);
        } else {
            rt_kprintf("lut:full\r\n");
            drv_epd_init(dev, EPD_LUT_UPDATE_FULL);
        }
        return 0;
    } else if(rt_strcmp(argv[2], "clear") == 0) {
        drv_epd_clear_frame_memory(dev, 0xFF); //0xFF:white, 0x00:black
    } else if(rt_strcmp(argv[2], "fill") == 0) {
        int data = atoi(argv[3]);
        rt_kprintf("fill 0x%x\r\n", data);
        drv_epd_clear_frame_memory(dev, data); //0xFF:white, 0x00:black
    } else if(rt_strcmp(argv[2], "point") == 0) {
        if(argc != 5) {
            rt_kprintf("point <x, y>\r\n");
            return 0;
        }
        int x = atoi(argv[3]);
        int y = atoi(argv[4]);
        rt_kprintf("draw point %d,%d\r\n", x, y);
        drv_epd_draw_color(dev, x, y, ~(1 << (x&0x7)));
    } else {
        epd_usage();
        return -1;
    }
    drv_epd_display_frame(dev);
    return 0;
}

MSH_CMD_EXPORT_ALIAS(epd_test, epd, test for epd.);
