/************************************************************
** @brief   : epd_paint_test
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-01
** @version : v1.0.0
** @note    : epd_paint_test.c
***********************************************************/
#include "epd_paint.h"
#include "drv_epd.h"
static uint8_t epd_paint_test_image[4096];
static void epd_usage(void)
{
    rt_kprintf("Usage: paint <dev> <cmd> [param]\r\n");
    rt_kprintf("\tclear <colored> <x,y>             - clear screen\r\n");
    rt_kprintf("\tset <width> <height> <rotate>     - set parameters\r\n");
    rt_kprintf("\tline <colored> <x0,y0,x1,y1>      - draw line\r\n");
    rt_kprintf("\tcircle <colored> <x0,y0,x1,y1>    - draw circle\r\n");
    rt_kprintf("\ttime <colored> <x,y>              - time display\r\n");
}
#include <time.h>
int epd_paint_test(int argc, char *argv[])
{
    static epd_paint_t paint = RT_NULL;
    /*{
            .buf = epd_paint_test_image,
            .width = EPD_WIDTH,//128,
            .height = EPD_HEIGHT,//64,
            .rotate = EPD_PAINT_ROTATE_270,//EPD_PAINT_ROTATE_0
            .invert_color = true
    };*/
    if(paint == RT_NULL) {
        paint = epd_paint_create(epd_paint_test_image, EPD_WIDTH, EPD_HEIGHT);
        if(paint == RT_NULL) {
            return -1;
        }
        epd_paint_set_rotate(paint, EPD_PAINT_ROTATE_270);
        epd_paint_set_invert_color(paint, true);
    }
    if(argc == 1) {
        epd_usage();
        return 0;
    }
    rt_device_t dev = rt_device_find(argv[1]);
    if(dev == RT_NULL) {
        rt_kprintf("%s not found!\r\n", argv[1]);
        return 0;
    }
    if(rt_strcmp(argv[2], "clear") == 0) {
        int colored = atoi(argv[3]);
        int x = atoi(argv[4]);
        int y = atoi(argv[5]);
        rt_kprintf("paint clear %d, %d,%d\r\n", colored, x, y);
        epd_paint_clear(paint, colored);
        drv_epd_set_frame_memory(dev, epd_paint_get_image(paint), x, y, epd_paint_get_width(paint), epd_paint_get_height(paint));
        drv_epd_display_frame(dev);
    } else if(rt_strcmp(argv[2], "set") == 0) {
        int w = atoi(argv[3]);
        int h = atoi(argv[4]);
        int r = atoi(argv[5]);
        rt_kprintf("paint w/h/r %d/%d/%d\r\n", w, h, r);
        epd_paint_set_rotate(paint, r);
        epd_paint_set_height(paint, h);
        epd_paint_set_width(paint, w);
    } else if(rt_strcmp(argv[2], "line") == 0) {
        int colored = atoi(argv[3]);
        int x0 = atoi(argv[4]);
        int y0 = atoi(argv[5]);
        int x1 = atoi(argv[6]);
        int y1 = atoi(argv[7]);
        rt_kprintf("paint line %d,%d,%d,%d,%d\r\n", colored, x0, y0, x1, y1);
        epd_paint_draw_line(paint, x0, y0, x1, y1, colored);
        drv_epd_set_frame_memory(dev, epd_paint_get_image(paint), 0, 0, epd_paint_get_width(paint), epd_paint_get_height(paint));
        drv_epd_display_frame(dev);
    } else if(rt_strcmp(argv[2], "circle") == 0) {
        int colored = atoi(argv[3]);
        int x = atoi(argv[4]);
        int y = atoi(argv[5]);
        int r = atoi(argv[6]);
        rt_kprintf("paint circle %d,%d,%d,%d\r\n", colored, x, y, r);
        epd_paint_draw_circle(paint, x, y, r, colored);
        drv_epd_set_frame_memory(dev, epd_paint_get_image(paint), 0, 0, epd_paint_get_width(paint), epd_paint_get_height(paint));
        drv_epd_display_frame(dev);
    } else if(rt_strcmp(argv[2], "time") == 0) {
        int colored = atoi(argv[3]);
        int x = atoi(argv[4]);
        int y = atoi(argv[5]);
        rt_kprintf("paint time %d,%d,%d\r\n", colored, x, y);
        rt_device_t cons = rt_console_get_device();
        if(cons == RT_NULL) {
            rt_kprintf("get console failed!\r\n");
            return -1;
        }
//        epd_paint_set_rotate(&paint, EPD_PAINT_ROTATE_270);
//        epd_paint_set_height(&paint, 256);
//        epd_paint_set_width(&paint, 32);
        time_t t = time(NULL);
        while(1) {
            const char *str_wday[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "SUN"};
            struct tm *ltm = localtime(&t);
            char str[32];//2022-02-07 11:53:00
            char ch;
            if(time(NULL) - t > 0) {
                t = time(NULL);
#if 0
                rt_sprintf(str, "%d-%02d-%02d %s", ltm->tm_year+1900, ltm->tm_mon+1, ltm->tm_mday, str_wday[ltm->tm_wday]);
                epd_paint_clear(&paint, !colored);
                epd_paint_draw_string_at(&paint, x, y, str, &font24, EPD_PAINT_ALIGN_CENTER, colored);
                drv_epd_set_frame_memory(dev, epd_paint_get_image(&paint), 0, 0, epd_paint_get_width(&paint), epd_paint_get_height(&paint));
                rt_sprintf(str, "%02d:%02d:%02d", ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
                epd_paint_clear(&paint, !colored);
                epd_paint_draw_string_at(&paint, x, y, str, &font24, EPD_PAINT_ALIGN_CENTER, colored);
#else
                epd_paint_clear(paint, !colored);
                rt_sprintf(str, "%d-%02d-%02d %s", ltm->tm_year+1900, ltm->tm_mon+1, ltm->tm_mday, str_wday[ltm->tm_wday]);
                epd_paint_draw_string_at(paint, x, y, str, &font24, EPD_PAINT_ALIGN_CENTER, colored);
                rt_sprintf(str, "%02d:%02d:%02d", ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
                epd_paint_draw_string_at(paint, x, y + 32, str, &font24, EPD_PAINT_ALIGN_CENTER, colored);
#endif
                drv_epd_set_frame_memory(dev, epd_paint_get_image(paint), 0, 0, epd_paint_get_width(paint), epd_paint_get_height(paint));
                drv_epd_display_frame(dev);
//                rt_kprintf("str:%s\r\n", str);
            }
            rt_thread_mdelay(500);
            if(rt_device_read(cons, -1, &ch, 1) == 1) {
                if(ch == 'q' || ch == 'Q' || ch == 0x03) {
                    return 0;
                } else {
                    rt_kprintf("%02x\r\n", ch);
                }
            }
        }
    } else {
        epd_usage();
        return -1;
    }
    return 0;
}

MSH_CMD_EXPORT_ALIAS(epd_paint_test, paint, test for epd paint.);
