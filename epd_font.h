/************************************************************
** @brief   : epd_font
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-01
** @version : v1.0.0
** @note    : epd_font.h
***********************************************************/
#ifndef __EPD_FONT_H__
#define __EPD_FONT_H__

#include <stdint.h>

struct epd_font {
  const uint8_t *table;
  uint16_t width;
  uint16_t height;
};

extern struct epd_font font12;
extern struct epd_font font24;

#endif /* __EPD_FONT_H__ */
