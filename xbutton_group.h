/************************************************************
** @brief   : xbutton_group
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-02
** @version : v1.0.0
** @note    : xbutton_group.h
***********************************************************/

#ifndef __XBUTTON_GROUP_H__
#define __XBUTTON_GROUP_H__

#include <rtthread.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    XBUTTON_EVENT_ALL = -1,
    XBUTTON_DOWN = 0,           //press
    XBUTTON_UP,                 //release
    XBUTTON_DOUBLE,             //double press
    XBUTTON_LONG,               //press long
    XBUTTON_LONG_FREE,
    XBUTTON_CONTINOUS,          //n press
    XBUTTON_CONTINOUS_FREE,
    XBUTTON_CONTINOUS_LONG,     //n long
    XBUTTON_CONTINOUS_LONG_FREE,
    XBUTTON_HOLD,               //hold
    XBUTTON_HOLD_FREE,
    XBUTTON_EVENT_MAX,
    XBUTTON_UNKNOWN
} xbutton_event_t;

#define IS_XBUTTON_EVENT(e)             ((e>=XBUTTON_EVENT_ALL)&&(e<XBUTTON_EVENT_MAX))

typedef struct _xbutton* xbutton_t;
typedef struct _xbutton_group* xbutton_group_t;
typedef uint32_t (*xbutton_get_ms_t) ();
typedef void (*xbutton_event_callback_t) (int index, xbutton_event_t evt);
typedef int (*xbutton_wait_event_t) (int *level);

const char *xbutton_event_code_to_string(xbutton_event_t evt);
xbutton_group_t xbutton_group_create(int btn_num, xbutton_get_ms_t get_ms, xbutton_wait_event_t wait_event);
void xbutton_group_destroy(xbutton_group_t grp);
uint32_t xbutton_group_get_long_press_time(xbutton_group_t grp);
int xbutton_group_set_long_press_time(xbutton_group_t grp, uint32_t t);
uint32_t xbutton_group_get_continous_press_time(xbutton_group_t grp);
int xbutton_group_set_continous_press_time(xbutton_group_t grp, uint32_t t);
uint32_t xbutton_group_get_hold_press_time(xbutton_group_t grp);
int xbutton_group_set_hold_press_time(xbutton_group_t grp, uint32_t t);
int xbutton_group_add(xbutton_group_t grp, int index, uint16_t trigger_level);
int xbutton_group_delete(xbutton_group_t grp, int index);
int xbutton_group_add_button_event(xbutton_group_t grp, int index, xbutton_event_t evt, xbutton_event_callback_t cb);
void xbutton_group_event_process(xbutton_group_t grp);

#ifdef __cplusplus
}
#endif

#endif

