/************************************************************
** @brief   : epd_paint
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-01
** @version : v1.0.0
** @note    : epd_paint.h
***********************************************************/

#ifndef __EPD_PAINT_H__
#define __EPD_PAINT_H__

#include <rtthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "epd_font.h"

#ifdef __cplusplus
extern "C" {
#endif

enum {
    EPD_PAINT_ROTATE_0 = 0,
    EPD_PAINT_ROTATE_90,
    EPD_PAINT_ROTATE_180,
    EPD_PAINT_ROTATE_270,
};

enum {
    EPD_PAINT_ALIGN_LEFT = 0,
    EPD_PAINT_ALIGN_RIGHT,
    EPD_PAINT_ALIGN_CENTER,
};

typedef struct _epd_paint * epd_paint_t;

epd_paint_t epd_paint_create(uint8_t *buf, int w, int h);
void epd_paint_delete(epd_paint_t paint);
void epd_paint_draw_absolute_pixel(epd_paint_t paint, int x, int y, int colored);
void epd_paint_clear(epd_paint_t paint, int colored);
uint8_t *epd_paint_get_image(epd_paint_t paint);
int epd_paint_get_width(epd_paint_t paint);
void epd_paint_set_width(epd_paint_t paint, int w);
int epd_paint_get_height(epd_paint_t paint);
void epd_paint_set_height(epd_paint_t paint, int h);
int epd_paint_get_rotate(epd_paint_t paint);
void epd_paint_set_rotate(epd_paint_t paint, int r);
int epd_paint_get_invert_color(epd_paint_t paint);
void epd_paint_set_invert_color(epd_paint_t paint, bool invert);
void epd_paint_draw_pixel(epd_paint_t paint, int x, int y, int colored);
void epd_paint_draw_char_at(epd_paint_t paint, int x, int y, char ascii_char, struct epd_font *font, int colored);
void epd_paint_draw_string_at(epd_paint_t paint, int x, int y, const char *text, struct epd_font *font, int align, int colored);
void epd_paint_draw_string_at_with_invert(epd_paint_t paint, int x, int y, const char *text, struct epd_font *font, int align, int colored, int begin, int end);
void epd_paint_draw_line(epd_paint_t paint, int x0, int y0, int x1, int y1, int colored);
void epd_paint_draw_horizontal_line(epd_paint_t paint, int x, int y, int line_width, int colored);
void epd_paint_draw_vertical_line(epd_paint_t paint, int x, int y, int line_height, int colored);
void epd_paint_draw_rectangle(epd_paint_t paint, int x0, int y0, int x1, int y1, int colored);
void epd_paint_fill_rectangle(epd_paint_t paint, int x0, int y0, int x1, int y1, int colored);
void epd_paint_draw_circle(epd_paint_t paint, int x, int y, int radius, int colored);
void epd_paint_fill_circle(epd_paint_t paint, int x, int y, int radius, int colored);

#ifdef __cplusplus
}
#endif

#endif /* __EPD_PAINT_H__ */
