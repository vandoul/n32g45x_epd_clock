/************************************************************
** @brief   : app_epd
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-01
** @version : v1.0.0
** @note    : app_epd.c
***********************************************************/

#include <rtdevice.h>
#include "xbutton.h"
#include "drv_epd.h"
#include "epd_paint.h"
#include "simple_page.h"
#include <time.h>

extern int simple_page_task_init(void);
extern void user_xbutton_task_init(void);

rt_event_t user_epd_evt;
void user_epd_task(void *param);
void user_epd_task_init()
{
    rt_thread_t tid;
    tid = rt_thread_create("epd_task", user_epd_task, RT_NULL, 4096, 10, 10);
    if(tid != RT_NULL) {
        rt_thread_startup(tid);
    }
}

static uint8_t epd_paint_test_image[4096];
const int epd_colored = 1;
epd_paint_t epd_paint;
/*{
        .buf = epd_paint_test_image,
        .width = EPD_WIDTH,//128,
        .height = EPD_HEIGHT,//64,
        .rotate = EPD_PAINT_ROTATE_270,//EPD_PAINT_ROTATE_0
        .invert_color = true
};*/
const char *str_wday[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "SUN"};
static void user_epd_paint_task(void *param)
{
    epd_paint_t paint = epd_paint;
    epd_paint_set_rotate(paint, EPD_PAINT_ROTATE_270);
    epd_paint_set_invert_color(paint, true);
    #if 1
    return ;
    #else
    rt_event_t evt = (rt_event_t) param;
    time_t t = 0;
    while(1) {
        char str[32];//2022-02-07 11:53:00
        time_t cur_tim = time(NULL);
        if((cur_tim - t > 60) || (cur_tim < t)) {
            t = cur_tim / 60 * 60;
            struct tm *ltm = localtime(&t);
            epd_paint_clear(paint, !epd_colored);
            rt_sprintf(str, "%d-%02d-%02d %s", ltm->tm_year+1900, ltm->tm_mon+1, ltm->tm_mday, str_wday[ltm->tm_wday]);
            epd_paint_draw_string_at(paint, 125, 32, str, &font24, EPD_PAINT_ALIGN_CENTER, epd_colored);
            rt_sprintf(str, "%02d:%02d", ltm->tm_hour, ltm->tm_min);
            epd_paint_draw_string_at(paint, 125, 64, str, &font24, EPD_PAINT_ALIGN_CENTER, epd_colored);
            rt_event_send(evt, 1);
        }
        rt_thread_mdelay(500);
    }
    #endif
}

static int user_epd_paint_task_init(void *param)
{
    rt_thread_t tid;
    tid = rt_thread_create("epd_paint", user_epd_paint_task, param, 4096, 10, 10);
    if(tid != RT_NULL) {
        rt_thread_startup(tid);
        return 0;
    }
    return -1;
}

static void user_epd_task(void *param)
{
#define REFRESH_NUMBER      100
    int refresh_cnt = 0;
    rt_device_t epd;
    epd = rt_device_find("epdif");
    if(epd == RT_NULL) {
        rt_kprintf("epd device not found.\r\n");
        return ;
    }
    epd_paint = epd_paint_create(epd_paint_test_image, EPD_WIDTH, EPD_HEIGHT);
    if(epd_paint == RT_NULL) {
        return ;
    }
    user_epd_evt = rt_event_create("epd_evt", RT_IPC_FLAG_FIFO);
    if(user_epd_evt == RT_NULL) {
        epd_paint_delete(epd_paint);
        return ;
    }
    if(user_epd_paint_task_init(user_epd_evt)) {
        rt_kprintf("paint task create failed.\r\n");
        rt_event_delete(user_epd_evt);
        epd_paint_delete(epd_paint);
        return ;
    }
    user_xbutton_task_init();
    simple_page_task_init();
    while(1) {
        if(rt_event_recv(user_epd_evt, 1, RT_EVENT_FLAG_OR|RT_EVENT_FLAG_CLEAR, RT_WAITING_FOREVER, RT_NULL) == RT_EOK) {
            if(refresh_cnt <= 0) {
                refresh_cnt = REFRESH_NUMBER;
                drv_epd_init(epd, EPD_LUT_UPDATE_FULL);
                drv_epd_clear_frame_memory(epd, !epd_colored);
                drv_epd_display_frame(epd);
                drv_epd_init(epd, EPD_LUT_UPDATE_PARTIAL);
            }
            refresh_cnt --;
            rt_thread_mdelay(100);
            drv_epd_set_frame_memory(epd, epd_paint_get_image(epd_paint), 0, 0, epd_paint_get_width(epd_paint), epd_paint_get_height(epd_paint));
            drv_epd_display_frame(epd);
        }
    }
}

