/************************************************************
** @brief   : simple_paint
** @author  : vandoul
** @github  : https://github.com/vandoul
** @date    : 2022-01
** @version : v1.0.0
** @note    : simple_paint.h
***********************************************************/
#ifndef __SIMPLE_PAINT_H__
#define __SIMPLE_PAINT_H__

#include <rtthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SIMPLE_PAGE_PAGE_NAME_SIZE          16
typedef enum {
    SIMPLE_PAGE_EVENT_ALL = -1,
    SIMPLE_PAGE_EVENT_DESTROY = 0,
    SIMPLE_PAGE_EVENT_ALARM,
    SIMPLE_PAGE_EVENT_NUM,
    SIMPLE_PAGE_EVENT_UNKNOWN
} simple_page_event_t;

typedef enum {
    SIMPLE_PAGE_PAGE_EVENT_ALL = -1,
    SIMPLE_PAGE_PAGE_EVENT_HOME = 0,
    SIMPLE_PAGE_PAGE_EVENT_PREV,
    SIMPLE_PAGE_PAGE_EVENT_NEXT,
    SIMPLE_PAGE_PAGE_EVENT_BACK,
    SIMPLE_PAGE_PAGE_EVENT_OK,
    SIMPLE_PAGE_PAGE_EVENT_IN,//5
    SIMPLE_PAGE_PAGE_EVENT_OUT,
    SIMPLE_PAGE_PAGE_EVENT_DRAW,
    SIMPLE_PAGE_PAGE_EVENT_DESTROY,
    SIMPLE_PAGE_PAGE_EVENT_NUM,
    SIMPLE_PAGE_PAGE_EVENT_UNKNOWN,
    _SIMPLE_PAGE_PAGE_EVENT_END,
} simple_page_page_event_t;


#define IS_SIMPLE_PAGE_PAGE_EVENT(e)            ((e>=SIMPLE_PAGE_PAGE_EVENT_ALL) && (e<SIMPLE_PAGE_PAGE_EVENT_NUM))
#define IS_SIMPLE_PAGE_EVENT(e)                 ((e>=SIMPLE_PAGE_EVENT_ALL) && (e<SIMPLE_PAGE_EVENT_NUM))

typedef struct _simple_page_page_link *simple_page_page_link_t;
typedef struct _simple_page_page *simple_page_page_t;
typedef struct _simple_page *simple_page_t;
typedef simple_page_page_event_t (*simple_page_event_wait_t)(void);
typedef void (*simple_page_page_event_callback_t)(simple_page_t page, simple_page_page_t page_page, simple_page_page_event_t evt, void *param);
typedef void (*simple_page_event_callback_t)(simple_page_t page, simple_page_event_t evt, void *param);

//page manager
void simple_page_page_add_next(simple_page_page_t page, simple_page_page_t next);
void simple_page_page_add_prev(simple_page_page_t page, simple_page_page_t prev);
void simple_page_page_add_child(simple_page_page_t page, simple_page_page_t child);
void simple_page_page_add_parent(simple_page_page_t page, simple_page_page_t parent);
simple_page_page_t simple_page_page_get_next(simple_page_page_t page);
simple_page_page_t simple_page_page_get_prev(simple_page_page_t page);
simple_page_page_t simple_page_page_get_child(simple_page_page_t page);
simple_page_page_t simple_page_page_get_parent(simple_page_page_t page);
// simple_page_page
simple_page_page_t simple_page_page_create(const char *name, simple_page_page_event_callback_t draw, void *fn_param);
void simple_page_page_destroy(simple_page_t page, simple_page_page_t page_page);
void simple_page_page_rename(simple_page_page_t page, const char *name);
const char *simple_page_page_get_name(simple_page_page_t page);
void simple_page_page_add_event(simple_page_page_t page, simple_page_page_event_t evt, simple_page_page_event_callback_t cb, void *param);
void simple_page_page_remove_event(simple_page_page_t page, simple_page_page_event_t evt);
// simple_page
void simple_page_process(simple_page_t page);
simple_page_t simple_page_create(simple_page_event_wait_t event_wait);
void simple_page_add_event(simple_page_t page, simple_page_event_t evt, simple_page_event_callback_t cb, void *param);
void simple_page_destroy(simple_page_t page);
void simple_page_remove_event(simple_page_t page, simple_page_event_t evt);
void simple_page_add_event_wait(simple_page_t page, simple_page_event_wait_t event_wait);
void simple_page_set_home_page_without_event(simple_page_t page, simple_page_page_t page_page);
void simple_page_set_home_page(simple_page_t page, simple_page_page_t page_page);
simple_page_page_t simple_page_get_home_page(simple_page_t page);
void simple_page_set_current_page_without_event(simple_page_t page, simple_page_page_t current_page);
void simple_page_set_current_page(simple_page_t page, simple_page_page_t current_page);
simple_page_page_t simple_page_get_current_page(simple_page_t page);
void simple_page_go_next_page(simple_page_t page);
void simple_page_go_prev_page(simple_page_t page);
void simple_page_go_child_page(simple_page_t page);
void simple_page_go_parent_page(simple_page_t page);

#ifdef __cplusplus
}
#endif

#endif
