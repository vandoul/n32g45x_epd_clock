#ifndef __N32G45X_BKP_REG_H__
#define __N32G45X_BKP_REG_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "n32g45x.h"

#define BKP_CTRL_TP_DISABLE             ((uint32_t)0x00000000)
#define BKP_CTRL_TP_ENABLE              ((uint32_t)0x00000001)

#define BKP_CTRL_TP_ALEV_DISABLE        ((uint32_t)0x00000000)
#define BKP_CTRL_TP_ALEV_ENABLE         ((uint32_t)0x00000001)

#define BKP         ((BKP_Module*)BKP_BASE)

#ifdef __cplusplus
}
#endif

#endif
