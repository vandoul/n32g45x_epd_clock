/************************************************************
** @brief   : simple_page_test
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-02
** @version : v1.0.0
** @note    : simple_page_test.c
***********************************************************/
#include <rtthread.h>
#include <rtdevice.h>
#include "simple_page.h"
#include "epd_paint.h"
#include "drv_epd.h"
#include "xbutton.h"

#define SMPLPAGE_EVENT_OK           (1<<0)
#define SMPLPAGE_EVENT_NEXT         (1<<1)
#define SMPLPAGE_EVENT_PREV         (1<<2)
#define SMPLPAGE_EVENT_BACK         (1<<3)
#define SMPLPAGE_EVENT_DRAW         (1<<4)
#define SMPLPAGE_EVENT_ALL          (SMPLPAGE_EVENT_OK|SMPLPAGE_EVENT_NEXT|SMPLPAGE_EVENT_PREV|SMPLPAGE_EVENT_BACK|SMPLPAGE_EVENT_DRAW)

extern rt_event_t simple_page_evt;
static simple_page_page_event_t simple_page_event_wait(void)
{
    simple_page_page_event_t ret = SIMPLE_PAGE_PAGE_EVENT_UNKNOWN;
    uint32_t evt;
    if(rt_event_recv(simple_page_evt, SMPLPAGE_EVENT_ALL, RT_EVENT_FLAG_OR, 20, &evt) == RT_EOK) {
        if(evt&SMPLPAGE_EVENT_OK) {
            rt_event_recv(simple_page_evt, SMPLPAGE_EVENT_OK, RT_EVENT_FLAG_AND|RT_EVENT_FLAG_CLEAR, 0, RT_NULL);
            ret = SIMPLE_PAGE_PAGE_EVENT_OK;
        } else if(evt&SMPLPAGE_EVENT_NEXT) {
            rt_event_recv(simple_page_evt, SMPLPAGE_EVENT_NEXT, RT_EVENT_FLAG_AND|RT_EVENT_FLAG_CLEAR, 0, RT_NULL);
            ret = SIMPLE_PAGE_PAGE_EVENT_NEXT;
        } else if(evt&SMPLPAGE_EVENT_PREV) {
            rt_event_recv(simple_page_evt, SMPLPAGE_EVENT_PREV, RT_EVENT_FLAG_AND|RT_EVENT_FLAG_CLEAR, 0, RT_NULL);
            ret = SIMPLE_PAGE_PAGE_EVENT_PREV;
        } else if(evt&SMPLPAGE_EVENT_BACK) {
            rt_event_recv(simple_page_evt, SMPLPAGE_EVENT_BACK, RT_EVENT_FLAG_AND|RT_EVENT_FLAG_CLEAR, 0, RT_NULL);
            ret = SIMPLE_PAGE_PAGE_EVENT_BACK;
        } else if(evt&SMPLPAGE_EVENT_DRAW) {
            rt_event_recv(simple_page_evt, SMPLPAGE_EVENT_DRAW, RT_EVENT_FLAG_AND|RT_EVENT_FLAG_CLEAR, 0, RT_NULL);
            ret = SIMPLE_PAGE_PAGE_EVENT_DRAW;
        }
    }
    return ret;
}
static void simple_clock_page_draw(simple_page_t page, simple_page_page_t page_page, simple_page_page_event_t evt, void *param)
{
    rt_kprintf("from simple_clock_page_draw\r\n");
}
static void simple_setting_page_draw(simple_page_t page, simple_page_page_t page_page, simple_page_page_event_t evt, void *param)
{
    rt_kprintf("from simple_setting_page_draw\r\n");
}
static void simple_page_event_home_callback(simple_page_t page, simple_page_page_t page_page, simple_page_page_event_t evt, void *param)
{
    rt_kprintf("from simple_page_event_home_callback %s,%d\r\n", simple_page_page_get_name(page_page), evt);
}
static int simple_page_test(int argc, char *argv[])
{
    rt_device_t cons = rt_console_get_device();
    if(cons == RT_NULL) {
        rt_kprintf("get console failed!\r\n");
        return -1;
    }
    simple_page_evt = rt_event_create("smplPage", RT_IPC_FLAG_FIFO);
    if(simple_page_evt == RT_NULL) {
        rt_kprintf("create smplPage failed!\r\n");
        return -1;
    }
    simple_page_t page;
    simple_page_page_t temp_page_page;
    simple_page_page_t page_page;
    page = simple_page_create(simple_page_event_wait);
    temp_page_page = page_page = simple_page_page_create("clock", simple_clock_page_draw, RT_NULL);
    simple_page_page_add_event(page_page, SIMPLE_PAGE_PAGE_EVENT_BACK, simple_page_event_home_callback, RT_NULL);
    if(page_page == RT_NULL) {
        simple_page_destroy(page);
    }
    simple_page_set_home_page(page, page_page);
    page_page = simple_page_page_create("setting", simple_setting_page_draw, RT_NULL);
    simple_page_page_add_next(temp_page_page, page_page);
    while(1) {
        char ch;
        simple_page_process(page);
        if(rt_device_read(cons, -1, &ch, 1) == 1) {
            if(ch == 'q' || ch == 'Q' || ch == 0x03) {
                break;
            } else {
                rt_kprintf("%02x\r\n", ch);
            }
        }
    }
    return 0;
}

MSH_CMD_EXPORT_ALIAS(simple_page_test, simple_page, test for simple_page.);
