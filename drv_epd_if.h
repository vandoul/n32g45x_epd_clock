/************************************************************
** @brief   : drv_epd_if
** @author  : vandoul
** @github  : https://gitee.com/vandoul
** @date    : 2022-01
** @version : v1.0.0
** @note    : drv_epd_if.h
***********************************************************/

#ifndef __DRV_EPD_IF_H__
#define __DRV_EPD_IF_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <rtthread.h>
#include <stdint.h>

#define EPD_SCK_PIN         52//PB13
#define EPD_DAT_PIN         54//PB15
#define EPD_CS_PIN          51//PB12
#define EPD_DC_PIN          45//PE14
#define EPD_RST_PIN         56//PD9
#define EPD_BUSY_PIN        47//PB10

// Display resolution
/* the resolution is 122x250 in fact */
/* however, the logical resolution is 128x250 */
#define EPD_WIDTH       128
#define EPD_HEIGHT      250

#define EPD_IF_CMD_RESET                        (128 + 0)
#define EPD_IF_CMD_SEND_DATA                    (128 + 1)
#define EPD_IF_CMD_SEND_COMMAND                 (128 + 2)
#define EPD_IF_CMD_WAIT_UNTIL_IDLE              (128 + 3)



#ifdef __cplusplus
}
#endif

#endif /* __DRV_EPD_IF_H__ */
