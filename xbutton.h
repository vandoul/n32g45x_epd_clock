/************************************************************
** @brief   : xbutton
** @author  : vandoul
** @github  : https://github.com/vandoul
** @date    : 2022-02
** @version : v1.0.0
** @note    : xbutton.h
***********************************************************/
#ifndef __XBUTTON_H__
#define __XBUTTON_H__

#include <rtthread.h>
#include <stdint.h>
#include <stdbool.h>
#include "xbutton_group.h"

#ifdef __cplusplus
extern "C" {
#endif

//xbutton
void xbutton_init(int btn_num, xbutton_get_ms_t get_ms, xbutton_wait_event_t wait_event);
void xbutton_deinit(void);
int xbutton_add(int index, uint16_t trigger_level);
int xbutton_delete(int index);
int xbutton_add_button_event(int index, xbutton_event_t evt, xbutton_event_callback_t cb);
void xbutton_process(void);

#ifdef __cplusplus
}
#endif

#endif
