/************************************************************
** @brief   : xbutton
** @author  : vandoul
** @github  : https://github.com/vandoul
** @date    : 2022-02
** @version : v1.0.0
** @note    : xbutton.c
***********************************************************/

#include "xbutton.h"

//xbutton_group instance
static xbutton_group_t xbtn_grp = RT_NULL;
//xbutton
void xbutton_init(int btn_num, xbutton_get_ms_t get_ms, xbutton_wait_event_t wait_event)
{
    if(xbtn_grp != RT_NULL) {
        xbutton_group_destroy(xbtn_grp);
    }
    xbtn_grp = xbutton_group_create(btn_num, get_ms, wait_event);
}

void xbutton_deinit(void)
{
    if(xbtn_grp != RT_NULL) {
        xbutton_group_destroy(xbtn_grp);
        xbtn_grp = RT_NULL;
    }
}

int xbutton_add(int index, uint16_t trigger_level)
{
    return xbutton_group_add(xbtn_grp, index, trigger_level);
}

int xbutton_delete(int index)
{
    return xbutton_group_delete(xbtn_grp, index);
}

int xbutton_add_button_event(int index, xbutton_event_t evt, xbutton_event_callback_t cb)
{
    return xbutton_group_add_button_event(xbtn_grp, index, evt, cb);
}

void xbutton_process(void)
{
    xbutton_group_event_process(xbtn_grp);
}


